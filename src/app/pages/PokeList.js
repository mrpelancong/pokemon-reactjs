import React, { useEffect } from 'react';
import ReactAnimateImages from "../../lib"
import axios from 'axios'
import { Grid, GridItem, Image, Button } from "@chakra-ui/react"
import { ArrowLeftIcon } from '@chakra-ui/icons'
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent, DrawerFooter,
  useToast 
} from "@chakra-ui/react"
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from "@chakra-ui/react"
import {
  FormControl, FormLabel, Input 
} from "@chakra-ui/react"
import { useDispatch, useSelector } from 'react-redux';

function PokeList() {
  const [ isOpenModal, setIsOpenModal] = React.useState(false)
  const [ nickName, setNickName] = React.useState('')
  // ---------------------------------------------------------
  const toast = useToast()
  const drawerState = useSelector(state => state.reducerDrawer)
  const dispatchDrawerState = useDispatch(state => state.reducerDrawer);

  const pokeState = useSelector(state => state.reducerPokeList)
  const dispatchPokeState = useDispatch(state => state.reducerPokeList);


  const pokemonPick = useSelector(state => state.reducerPokemon)
  const dispatchPokemonPick = useDispatch(state => state.reducerPokemon)

  const dispatchPokemonGet = useDispatch(state => state.reducerMyPoke)

  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  const getPokemon = (lepaskan) => {
    let num = getRandomInt(10); // mengambil int secara acak 1 - 10 
    let divNumber = num%2;
    if (lepaskan) {
      toast({
        title: pokemonPick.name + " Dilepaskan",
        status: "warning",
        duration: 3000,
        isClosable: true,
      })
      dispatchPokemonGet({
        type : 'REALEASE_POKEMON',
        indexMy : pokemonPick.indexMy
      })
      closeDrawer();
    } else {
      console.log('Get Random Number ',num);
      console.log('Div Number ', divNumber);
      if (divNumber === 0) {
        toast({
          title: "Selamat! Anda Mendapat Pokemon",
          status: "success",
          position: 'top',
          duration: 3000,
          isClosable: true,
        });
        setIsOpenModal(true);      
      } else {
        toast({
          title: "Anda Belum Beruntung",
          status: "error",
          duration: 3000,
          isClosable: true,
        })
      }
    }
  }

  const simpanPokemon = (poke) => {
    poke = {
      ...poke,
      nickName : nickName
    }
    dispatchPokemonGet({
        type : 'GET_POKEMON',
        pokemon : poke
      })
    setIsOpenModal(false);
  }
  
  // ---- Loading Pokemon 
  const loadPokemon = () => {
    let tmpRes = [];
    axios.get(pokeState.next)
    .then(function (response) {
      // handle success
      response.data.results.forEach((data, index) => {
        axios.get(data.url).then((responseDetail) => {

          tmpRes.push({
            ...response.data.results[index],
            picture : responseDetail.data.sprites.other.dream_world.front_default
          })
        }).catch((error) => {
          console.log(error)
        }).finally(() => {
          if (response.data.results.length === tmpRes.length) {
            console.log(tmpRes)
            dispatchPokeState({
              type : 'PUT_POKEMON',
              res : {
                next : response.data.next,
                data : tmpRes
              }
            }) 
          }
        })
      });
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
  }

  // ----- Pick Pokemon
  const selectPoke = (url) => {
    return axios.get(url)
      .then(function (response) {
        dispatchPokemonPick({
          type : 'SET_POKEMON',
          res : response.data,
          status : true,
        })
        dispatchDrawerState({
          type: 'ACT_DRAWER',
          val : true
        });
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  const closeDrawer = () => {
    dispatchDrawerState({
      type : 'ACT_DRAWER',
      val : false
    })
  }

  const ListPoke = ({data}) => {
    return (
      <div>
        {data.map((poke, index) => (
          <Grid
            h="50px"
            className="listPoke"
            templateRows="repeat(2, 1fr)"
            templateColumns="repeat(5, 1fr)"
            gap={4}
            onClick={() => selectPoke(poke.url)}
            key={index}
            >
            <GridItem rowSpan={2} colSpan={1}  >
              <Image
                boxSize="50px"
                borderRadius="full"
                src={poke.picture}
              />
            </GridItem>
            <GridItem 
              rowSpan={2} 
              colSpan={4}
              className="nama_pokemon"  
            >
              {poke.name}
            </GridItem>
          </Grid>
          ))}
      </div>
    )
  }

  useEffect(() => {
    
    return () => {
      setIsOpenModal(false);
      setNickName('')
    }
  }, [])

  return (
    <div>
      <ListPoke data={pokeState.results} />
      
      <Button colorScheme="teal" variant="solid" isFullWidth={true} onClick={() => loadPokemon()}>
        Load Pokemon
      </Button>
      {/* View Pokemon dengan Drawer */}
      <Drawer isOpen={drawerState.isOpenDrawer} size="full">
        <DrawerOverlay>
          <DrawerContent>
            <ArrowLeftIcon w={4} h={4} ml={3} mt={3} onClick={() => closeDrawer()} />  
            <DrawerHeader mt={0} pt={0} className="nama_pokemon_view" w="100%">{pokemonPick.name} - {pokemonPick.types} </DrawerHeader>
            <DrawerBody>
            <Grid
              h="100%"
              templateRows="repeat(3, 1fr)"
              templateColumns="repeat(5, 1fr)"
              gap={4}
              >
                <GridItem 
                  className="poke_view"
                  rowSpan={1} 
                  colSpan={5}  >
                    <ReactAnimateImages
                      style={{ heigth: "150px" }}
                      images={pokemonPick.status ? pokemonPick.movePic : pokemonPick.picture } 
                      framInterval={200}
                      stopAct={pokemonPick.stopAct} 
                      stopAfterFirstRound={false}
                    />
                </GridItem>
                <GridItem
                  rowSpan={1}
                  colSpan={5}
                >
                  <Table size="sm">
                    <Thead>
                      <Tr>
                        <Th>name</Th>
                        <Th isNumeric>base_stat</Th>
                        <Th isNumeric>effort</Th>
                      </Tr>
                    </Thead>
                    <Tbody>
                      {pokemonPick.stats.map((poke, index) => (
                        <Tr key={index}>
                          <Td >{poke.stat.name}</Td>
                          <Td isNumeric>{poke.base_stat}</Td>
                          <Td isNumeric>{poke.effort}</Td>
                        </Tr>
                      ))}
                    </Tbody>
                  </Table>
                </GridItem>
                <GridItem 
                  rowSpan={2} 
                  colSpan={5}
                >
                  <Button colorScheme="teal" variant="solid" isFullWidth={true} onClick={() => getPokemon(pokemonPick.lepaskan)} >
                    {pokemonPick.lepaskan ? 'Lepaskan Pokemon' : 'Tangkap Pokemon'}
                  </Button>
                </GridItem>
            </Grid>
            </DrawerBody>
          </DrawerContent>
        </DrawerOverlay>
      </Drawer>
      {/* Modal Get Pokemon */}
      <Drawer placement="bottom" isOpen={isOpenModal}>
        <DrawerOverlay>
          <DrawerContent>
            <DrawerHeader borderBottomWidth="1px">Yeay! Pokemon Baru</DrawerHeader>
            <DrawerBody>
              <FormControl>
                <FormLabel>Nick name</FormLabel>
                <Input onChange={e => setNickName(e.target.value)} placeholder="Nick name" />
              </FormControl>
            </DrawerBody>
            <DrawerFooter borderBottomWidth="1px">
              <Button colorScheme="blue" mr={3} onClick={() => simpanPokemon(pokemonPick)}>
                Simpan Pokemon
              </Button>
              <Button onClick={() => setIsOpenModal(false)}>Lepaskan</Button>
            </DrawerFooter>
          </DrawerContent>
        </DrawerOverlay>
      </Drawer>
      
    </div>
  )
}

export default PokeList