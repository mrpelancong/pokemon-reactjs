import React from 'react';
import { Grid, GridItem, Image } from "@chakra-ui/react"
import { useDispatch, useSelector } from 'react-redux';

function MyPoke() {
  const MypokeState = useSelector(state => state.reducerMyPoke)

  const dispatchPokemonPick = useDispatch(state => state.reducerPokemon);
  const dispatchDrawerState = useDispatch(state => state.reducerDrawer);

  const selectPoke = (index) => {
    dispatchPokemonPick({
      type : 'SETMY_POKEMON',
      res : MypokeState.results[index],
      indexMy : index,
      status : true,
    })
    dispatchDrawerState({
      type: 'ACT_DRAWER',
      val : true
    });
  }
  const ListPoke = ({data}) => {
    return (
      <div>
        {data.map((poke, index) => (
          <Grid
            h="50px"
            className="listPoke"
            templateRows="repeat(2, 1fr)"
            templateColumns="repeat(5, 1fr)"
            gap={4}
            onClick={() => selectPoke(index)}
            key={index}
            >
            <GridItem rowSpan={2} colSpan={1}  >
              <Image
                boxSize="50px"
                borderRadius="full"
                fallbackSrc="https://github.com/gaurav-gogia/pakdemon/blob/master/includes/pokeball.gif?raw=true"
                src={poke.picture}
              />
            </GridItem>
            <GridItem 
              rowSpan={2} 
              colSpan={4}
              className="nama_pokemon"  
            >
              {poke.name} - {poke.nickName}
            </GridItem>
          </Grid>
          ))}
      </div>
    )
  }

  return (
    <div>
      <ListPoke data={MypokeState.results} />
    </div>
  );
}

export default MyPoke