const BASE_URL = "http://localhost:3026/";

const API_URL = {
    pokemon : BASE_URL + "pokemon/",
    type    : BASE_URL + "type/",
    ability : BASE_URL + "ability/"
};

export const api = API_URL;
