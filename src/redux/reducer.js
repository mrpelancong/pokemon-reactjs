import { combineReducers } from 'redux'
import { api } from '../const'

// -------
const initialStateDrawer = {
    isOpenDrawer : false,
}
const reducerDrawer = (state = initialStateDrawer, action) => {
    if (action.type === 'ACT_DRAWER') {
        return {
            ...state,
            isOpenDrawer : action.val
        } 
    }

    return state;
}

// -------
const initialStateMyPoke = {
    results : []
}
const reducerMyPoke = (state = initialStateMyPoke, action) => {
    if (action.type === 'GET_POKEMON') {
        return {
            ...state,
            results : [...state.results, action.pokemon]
        } 
    }

    if (action.type === 'REALEASE_POKEMON') {
        if (action.indexMy > -1) {
            state.results.splice(action.indexMy, 1)
            return {
                ...state
            };
        }
    }

    return state;
}

// ---------
const initialStatePokeList = {
    start: '',
    next : api.pokemon+'?limit=5&offset=0',
    previous : '',
    results : []
}
const reducerPokeList = (state = initialStatePokeList, action) => {
    if (action.type === 'PUT_POKEMON') {
        action.res.data.forEach(data => {
            state.results.push(data);
        });
        return {
            ...state,
            next : action.res.next,
        }
    }
    return state;
}


// ----- Detail Pokemon
const initialStatePokemon = {
    name : '',
    picture : 'https://github.com/gaurav-gogia/pakdemon/blob/master/includes/pokeball.gif?raw=true',
    stats : []  ,
    movePic : ['https://github.com/gaurav-gogia/pakdemon/blob/master/includes/pokeball.gif?raw=true'],
    types : '',
    status : false,
    stopAct : false,
    lepaskan : false,
    indexMy : null
}
const reducerPokemon = (state = initialStatePokemon, action) => {
    if (action.type === 'SET_POKEMON') {
        return {
            ...state.initialStatePokemon,
            name : action.res.name,
            picture  : action.res.sprites.other.dream_world.front_default,
            stats : action.res.stats,
            types : action.res.types[0].type.name,
            status : action.status,
            stopAct : false,
            movePic : [action.res.sprites.back_default, action.res.sprites.back_female, action.res.sprites.back_shiny, action.res.sprites.back_shiny_female, action.res.sprites.front_default, action.res.sprites.front_female, action.res.sprites.front_shiny, action.res.sprites.front_shiny_female]
        }
    }
    if (action.type === 'SETMY_POKEMON') {
        return {
            ...state.initialStatePokemon,
            name : action.res.name,
            picture  : action.res.picture,
            stats : action.res.stats,
            types : action.res.types,
            status : action.status,
            stopAct : false,
            movePic : action.res.movePic,
            lepaskan : true,
            indexMy : action.indexMy
        }
    }
    if (action.type === 'CLEAR_POKEMON') {
        return {
            ...state,
            name : '',
            picture  : 'https://github.com/gaurav-gogia/pakdemon/blob/master/includes/pokeball.gif?raw=true',
            stats : [],
            types : '',
            status : false,
            stopAct : true,
            movePic : ''
        }
    }
    return state;
}

const reducer = combineReducers({
    reducerPokeList, reducerMyPoke, reducerPokemon, reducerDrawer
})

export default reducer;