import React from 'react';
import PokeList from './app/pages/PokeList'
import MyPoke from './app/pages/MyPoke'
import { Tabs, TabList, TabPanels, Tab, TabPanel, HStack } from "@chakra-ui/react"

import './App.css';

function App() {
  function DataTabs() {
    return (
      <Tabs isFitted>
        <TabList style={{
          position : 'sticky',
          backgroundColor : 'white',
          top: 0
        }}>
          <Tab>List Pokemon</Tab>
          <Tab>My Pokemon</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <PokeList />
          </TabPanel>
          <TabPanel>
            <MyPoke />
          </TabPanel>
        </TabPanels>
      </Tabs>
    )
  }


  // 3. Pass the props and chill!
  return <DataTabs />
}

export default App;
